package com.example.demo.test1;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

/**
 * Created by wanghaizhou on 2018/4/9.
 */
@Component
public class China {

    @Resource(name="steelAxe")
    SteelAxe steelAxe;


    public China() {
        System.out.println("创建China对象");
    }

    @PostConstruct
    private void init(){
        System.out.println("创建China对象前操作");
    }

    @PreDestroy
    private void close(){
        System.out.println("销毁China对象后操作");
    }

}
