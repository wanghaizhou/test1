package com.example.demo.spring.customelement;

import org.springframework.beans.factory.config.FieldRetrievingFactoryBean;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.*;
import org.springframework.util.StringUtils;
import org.w3c.dom.Element;

/**
 * Created by wanghaizhou on 2018/4/10.
 */
public class MyNamespaceHandler extends NamespaceHandlerSupport {

    @Override
    public void init() {
        registerBeanDefinitionParser("user", new MyNamespaceHandler.UserBeanDefinitionParser());
    }

    private static class UserBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {

        protected Class<?> getBeanClass(Element element) {
            return User.class;
        }

        protected void doParse(Element element, BeanDefinitionBuilder builder) {
            String name = element.getAttribute("name");
            String email = element.getAttribute("email");
            if (StringUtils.hasText(name)){
                builder.addPropertyValue("name",name);
            }

            if (StringUtils.hasText(email)){
                builder.addPropertyValue("email",email);
            }
        }
    }
}
