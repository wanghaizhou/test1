package com.example.demo.spring.customelement;

import com.example.demo.spring.chapter2.helloworld.Car;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * Created by wanghaizhou on 2018/4/9.
 */

public class SimpleBean {

    @Test
    public void test(){
        BeanFactory beanFactory  = new XmlBeanFactory(new ClassPathResource("user.xml"));
        User user = (User) beanFactory.getBean("testuser");
        System.out.println(user);
    }


}
