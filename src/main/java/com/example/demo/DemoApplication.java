package com.example.demo;

import com.example.demo.test1.China;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;


@SpringBootApplication
public class DemoApplication {

	@Resource(name = "china")
	China china;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		/*System.out.println(new DemoApplication().china);*/
		// 创建Spring容器
		AbstractApplicationContext ctx = new
				ClassPathXmlApplicationContext("beans.xml");
		// 注册关闭钩子
		ctx.registerShutdownHook();
	}
}
